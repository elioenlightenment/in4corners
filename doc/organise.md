# Organise

If you move demo files you can reset `gallery` like this:

```
function exifish
  set SPLARGS (string split " " $argv)
  set EXIFKEY $SPLARGS[1]
  set IMAGEFILE (string join " " $SPLARGS[2..-1])
  echo (string split ": " (exiftool -m -s $IMAGEFILE -$EXIFKEY))[2]
end

for IMG in (find . -name "*.JPG" -type f)
  set SPLITTED (string split "/" "$IMG")
  set DIR (string join "/" $SPLITTED[1..-2])
  set FIL $SPLITTED[-1]
  set EQP (exifish Software "$IMG")
  set MVFOL $EQP
  echo $MVFOL
  mkdir -p "$MVFOL"
  mv -n "$IMG" "$MVFOL/$FIL"
end
find . -type d -empty -delete
```
