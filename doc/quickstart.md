# Quickstart in4corners

- [express-flesh Prerequisites](/elioflesh/express-flesh/prerequisites.html)
- [Installing express-flesh](/elioflesh/express-flesh/installing.html)

## Nutshell

You will learn:

1. Create the app.
2. Organise your photographs.
3. **Engage** a website.
4. **List** galleries.
5. **Iterate** to **Engage** each gallery.
6. **List** photos.
7. **List** links.
8. Run the app.
9. **Optimize** the app.
10. Deploy the app.

## Create the app (or how they made me)

```
cd ~/MyDevFolder/elioway/elioenlightenment/
yo flesh:express-flesh --identifier in4corners
cd in4corners
yo angels:artwork
```

## Organise your photographs.

> I, Tim Bushell, hearby and forthwith, officially, in sound mind, am putting these into the public domain.

The demo gallery contains photos taken using the Hipstamatic App on the iPhone 3s, in Thailand, by the creator of the **elioWay**, Tim Bushell.

`gallery` sub folders are the names of the Hipstamatic lens, film and flash used to take the photo.

- `Americana Lens, Blanko Film, No Flash`
- `Helga Viking Lens, BlacKeys SuperGrain Film, No Flash`
- `Helga Viking Lens, Blanko Film, No Flash`
- `Helga Viking Lens, Ina's 1935 Film, Laser Lemon Gel Flash`
- `Jimmy Lens, Blanko Film, No Flash`
- `John S Lens, Blanko Film, Laser Lemon Gel Flash`
- `John S Lens, Blanko Film, No Flash`
- `Lucifer VI Lens, Pistil Film, No Flash`
- `Melodie Lens, Pistil Film, No Flash`

I organised them [using this script](organise.html)

## Database Setup and Editing

We're doing the things the elioWay. Hopefully you already know [**the elioWay** has simple laws](/a-cult-not-a-framework.html) and the first is: You can't **list** until you have **engaged** a `Thing`.

So let's engage the first thing, which will represent the home page of our photo gallery.

You need to choose an identifier which is also domain friendly. For this QuickStart we will use as `in4corners` which you can replace with the "name-of-your-website"

### Setup

```
function titlelize
  echo (string join "" (string match -r -a "[a-zA-Z0-9]" (string collect $argv)))
end
set APPROOT ~/Dev/elioenlightenment/in4corners
set APPIDENTIFIER in4corners  
set SHAREDPROPS "--CreativeWork.contentLocation=$APPROOT/gallery"
```

### Engage Home Page

```
cd $APPROOT

rm -rf .data

# Home page
npm run in4corners -- takeupT $APPIDENTIFIER --mainEntityOfPage=ImageGallery --name=in4corners

# Don't require auth access (it's only file system anyway).
npm run in4corners -- anonifyT $APPIDENTIFIER

# Remove fields that are empty from the initialised thing.
npm run in4corners -- cleanT $APPIDENTIFIER
```

Now run `npm run dev` (in another terminal) to start the server and open `http://localhost:3000`. It will be a blank page.

Let's add a "name"

```
npm run in4corners -- updateT $APPIDENTIFIER --name="In 4 Corners"
```

Refresh the browser page.

Now we can start adding a gallery of images

### Home page images.

```
set PHOTOIMAGE "Americana Lens, Blanko Film, No Flash/2012-10-05-click-31994380614.JPG"
set PHOTOIDENTIFIER click

npm run in4corners -- takeupT $APPIDENTIFIER $PHOTOIDENTIFIER --mainEntityOfPage=ImageObject 

npm run in4corners -- anonifyT $PHOTOIDENTIFIER

npm run in4corners -- updateT $PHOTOIDENTIFIER --MediaObject.contentUrl=$PHOTOIMAGE --image=$APPROOT/media/$PHOTOIMAGE 

npm run in4corners -- exifyT $PHOTOIDENTIFIER
npm run in4corners -- cleanT $PHOTOIDENTIFIER
npm run in4corners -- readT $PHOTOIDENTIFIER
```

### Engage Galleries List Photos

```
cd $APPROOT/gallery

for GALLERY in (ls -1)

  set GALLERYIDENTIFIER (titlelize $GALLERY)
  set GALLERYIMAGE "notset"

  # elioList the photos in the gallery folder.
  cd $APPROOT/gallery/$GALLERY

  for PHOTO in (ls -1)
    set PHOTOIDENTIFIER (titlelize $PHOTO)
    set PHOTOIMAGE $GALLERY/$PHOTO

    # Take up this photo as a database record. This creates a file in the system. `takeonT` just adds it as a list?
    npm run in4corners -- takeupT $PHOTOIDENTIFIER --mainEntityOfPage=ImageObject --MediaObject.contentUrl=$PHOTOIMAGE --image=$APPROOT/gallery/$PHOTOIMAGE --subjectOf=$GALLERYIDENTIFIER
    npm run in4corners -- anonifyT $PHOTOIDENTIFIER
    npm run in4corners -- cleanT $PHOTOIDENTIFIER

    # Suck in EXIF fields into Schema.org fields.
    npm run in4corners -- exifyT $PHOTOIDENTIFIER

    # Add this to the $GALLERY list.
    npm run in4corners -- enlistT $GALLERYIDENTIFIER $PHOTOIDENTIFIER

    # Use the first image you find as the gallery's image.
    if test $GALLERYIMAGE="notset"
      set GALLERYIMAGE "isset"

      npm run in4corners -- updateT $GALLERYIDENTIFIER --image=$PHOTOIMAGE

      # Add the gallery image to the home page image list (just to have more than links).
      npm run in4corners -- enlistT $APPIDENTIFIER $PHOTOIDENTIFIER
    end
  end

end
```

### Test

Next we run the website.

```
npm i
npm run serve
```

## Data changes

Some of these galleries have only one photo. We will remove links to them.

```
for GALLERY in (
  "Americana Lens, Blanko Film, No Flash"
  "Helga Viking Lens, Ina's 1935 Film, Laser Lemon Gel Flash"
  "John S Lens, Blanko Film, Laser Lemon Gel Flash"
  "Lucifer VI Lens, Pistil Film, No Flash"
)
set GALLERYIDENTIFIER (titlelize $GALLERY)
npm run in4corners -- unlistT $APPIDENTIFIER $GALLERYIDENTIFIER
```

One of the images in the home page list is also the home image.

```
for GALLERY in (
  "Americana Lens, Blanko Film, No Flash"
  "Helga Viking Lens, Ina's 1935 Film, Laser Lemon Gel Flash"
  "John S Lens, Blanko Film, Laser Lemon Gel Flash"
  "Lucifer VI Lens, Pistil Film, No Flash"
)
set PHOTOIDENTIFIER (titlelize "Americana Lens, Blanko Film, No Flash/2012-10-05-click-31994380614.JPG")
npm run in4corners -- unlistT $APPIDENTIFIER $PHOTOIDENTIFIER
```

## Make then in4

In order to make them into 4sq, we will move some to a sub gallery

```
cd gallery
for $GALLERY in (ls -1)
  set GALLERYIDENTIFIER (titlelize $GALLERY)
  set GALLERYIMAGE "notset"

  # A sub gallery. We'll give it the same name.
  npm run in4corners -- takeupT moreThan4$GALLERYIDENTIFIER --mainEntityOfPage=ImageGallery --name=$GALLERY
  npm run in4corners -- anonifyT moreThan4$GALLERYIDENTIFIER
  npm run in4corners -- cleanT moreThan4$GALLERYIDENTIFIER

  # A sub gallery. We'll link to it from the main gallery.
  npm run in4corners -- takeonT $GALLERYIDENTIFIER link2moreThan4$GALLERYIDENTIFIER --mainEntityOfPage=EntryPoint --name="More than 4" --url=moreThan4$GALLERYIDENTIFIER

  # We'll link back to the main gallery.
  npm run in4corners -- takeonT link2moreThan4$GALLERYIDENTIFIER $GALLERYIDENTIFIER --mainEntityOfPage=EntryPoint --name=$GALLERY --url=$GALLERYIDENTIFIER

  # Add every photo to the sub gallery like we did to the main gallery. You'll see why.
  cd $GALLERY

  for $PHOTO in (ls -1)
    set PHOTOIDENTIFIER (titlelize $PHOTO)
    set PHOTOIMAGE $GALLERY/$PHOTO

    # Just enlist it.
    npm run in4corners -- enlistT moreThan4$GALLERYIDENTIFIER $PHOTOIDENTIFIER

    # Use the first image you find as the sub gallery's image.
    if test $GALLERYIMAGE="notset"
      set GALLERYIMAGE "isset"
      npm run in4corners -- updateT moreThan4$GALLERYIDENTIFIER --image=$PHOTOIMAGE
    end
  end

  cd ..

end
```

Now you have a link to a sub folder with all the images. You can now hand remove from the main gallery until you have 4 or 9 in each. That will be the in4corners way. And you can switch between the 4 and the rest so we can see what you're curating out!

## Writing the `cloneT` rib

### You'll learn to:

- Use "yo:ribs" to create a new rib
- Add the endpoint to your npm run in4corners -- CLI.

## Writing the `likeT` rib

### You'll learn to:

- Use "yo:ribs" to create a new rib
- Add the endpoint to your flesh server and handle the "like" button click.
