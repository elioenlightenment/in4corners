import reload from "reload";
import db from "@elioway/dbhell";
import elioMadeFlesh from "@elioway/express-flesh";
import { adons, boneEnvVarsLoader, ribs, spine } from "@elioway/bones";

const commandDir = process.cwd();
let CFG = boneEnvVarsLoader(true, "", commandDir);

elioMadeFlesh(CFG, { ...ribs, ...spine, ...adons }, db).then((app) =>
  reload(app)
    .then((r) =>
      app.listen(CFG.PORT, () =>
        console.log(`elioFlesh express server listening on ${CFG.PORT}`)
      )
    )
    .catch((err) =>
      console.error("Reload could not start elioFlesh express server.", err)
    )
);
