#!/usr/bin/env node
import * as fs from "fs";
import db from "@elioway/dbhell";
import exifyT from "@elioway/exifyt";
import {
  adons,
  boneUp,
  boneEnvVarsLoader,
  flesh,
  ribs,
  ribsConfig,
  spine,
  yargsBone,
} from "@elioway/bones";

fs.readFile(".env", "utf8", (readEnvErr, envData) => {
  const CFG = boneEnvVarsLoader(readEnvErr, envData, process.cwd() )
  const allDefaultRibs = { ...ribs, ...spine, ...adons, exifyT };
  const allRibsConfig = {
    ...ribsConfig,
    exifyT: { aliases: ["exif"], positionals: ["identifier"] },
  }
  let commandHandler = (packet) => boneUp(packet, allDefaultRibs, db, flesh);
  db.initialize(CFG, () => yargsBone(CFG.APPIDENTIFIER, allRibsConfig, commandHandler))
})
