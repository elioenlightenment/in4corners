![](https://elioway.gitlab.io/Pictures/in4corners/elio-in-4-corners-logo.png)

> Travel Photography, **the elioWay**

# in4corners

A simple photo gallery with instructions for anyone to build something.

- [express-flesh Documentation](https://elioway.gitlab.io/Pictures/express-flesh/)

## Prerequisites

- [express-flesh Prerequisites](https://elioway.gitlab.io/Pictures/express-flesh/installing.html)

## Installing

- [Installing in4corners](https://elioway.gitlab.io/elioflesh/express-flesh/installing.html)

## Seeing is Believing

```
set APPROOT ~/Dev/elioenlightenment/in4corners
set APPIDENTIFIER in4corners

cd $APPROOT
                    
rm -rf .data

# Home page
set IDENTIFIER $APPIDENTIFIER

bones takeupT $IDENTIFIER --mainEntityOfPage=ImageGallery 
bones anonifyT $IDENTIFIER
bones updateT $IDENTIFIER --name="in4corners"
bones updateT $IDENTIFIER --alternateName="the elioWay"
bones updateT $IDENTIFIER --description="Taken using the Hipstamatic App on the iPhone 3s, in Thailand, by the creator of the elioWay, Tim Bushell"
bones updateT $IDENTIFIER --disambiguatingDescription="A photo gallery system built out of Things"
bones updateT $IDENTIFIER --image=""
bones updateT $IDENTIFIER --name=""
bones updateT $IDENTIFIER --potentialAction=""
bones updateT $IDENTIFIER --sameAs=""
bones updateT $IDENTIFIER --subjectOf="elioWay"
bones updateT $IDENTIFIER --url="http://in4corners.theElioWay.net/"



set PHOTOIDENTIFIER 2011-01-18-pet-parrot-koh-samui-thailand-9155501391.JPG

bones takeonT in-4-corners $PHOTOIDENTIFIER --mainEntityOfPage=ImageObject
bones anonifyT $PHOTOIDENTIFIER
bones updateT $PHOTOIDENTIFIER --MediaObject.height=1936 --MediaObject.width=1936 --MediaObject.contentUrl=$PHOTOIDENTIFIER --image=$APPROOT/media/$PHOTOIDENTIFIER
bones cleanT $PHOTOIDENTIFIER
bones readT $PHOTOIDENTIFIER


```

- [elioflesh Quickstart](https://elioway.gitlab.io/elioflesh/quickstart.html)
- [express-flesh Quickstart](https://elioway.gitlab.io/elioflesh/express-flesh/quickstart.html)

# Credits

- [in4corners Credits](https://elioway.gitlab.io/elioflesh/express-flesh/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioflesh/express-flesh/apple-touch-icon.png)
